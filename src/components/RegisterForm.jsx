import React, { useState } from 'react';
import Data from './Data';

const RegisterForm = () => {
    const [fullname, setFullname] = useState('');
    const [age, setAge] = useState(0);
    const [pokemon, setPokemon] = useState('');
    const [data, setData] = useState([]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        if (name === "fullname") {
            setFullname(value);
        } else if (name === "age") {
            setAge(value);
        } else if (name === "pokemon") {
            setPokemon(value);
        }
    }
    
    const validateNameAndPokemon = (fullname, pokemon) => {
        const withoutSpacesName = fullname.trim();
        const withoutSpacesPokemon = pokemon.trim();
        return withoutSpacesName.length === 0 || withoutSpacesPokemon.length === 0;;
    }

    const validateAge = (age) => {
        const parsedAge = parseInt(age);
        return isNaN(parsedAge) || parsedAge < 1;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log( fullname, age, pokemon );
        if(validateNameAndPokemon(fullname, pokemon)) {
            alert('Nombre o pokemon inválido');
            return;
        }
        if(validateAge(age)) {
            alert('Edad inválida');
            return;
        }
        setData([{ fullname, age, pokemon }]);
        setFullname('');
        setAge(0);
        setPokemon('');
    }

  return (
    <div>
        <h2>Registro de Usuario</h2>
        <form onSubmit={handleSubmit}>
            <div>
                <label>Nombre completo:</label>
                <input
                    type="text"
                    name="fullname"
                    value={fullname}
                    onChange={handleInputChange}
                />
            </div>

            <div>
                <label>Edad:</label>
                <input
                    type="number"
                    name="age"
                    value={age}
                    onChange={handleInputChange}
                />
            </div>

            <div>
                <label>Pokemon favorito:</label>
                <input
                    type="text"
                    name="pokemon"
                    value={pokemon}
                    onChange={handleInputChange}
                />
            </div>
            <button type="submit">Registrar</button>
        </form>
        <br />
        <Data data={data} />
    </div>
  )
}

export default RegisterForm;